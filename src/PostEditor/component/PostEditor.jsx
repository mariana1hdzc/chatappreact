import React, { Component } from 'react';


class PostEditor extends Component {

    constructor(props) {
    super(props);

       // hack para asegurar que las funciones tiene el contexto del componente

    this.handlePostEditorInputChange = this.handlePostEditorInputChange.bind(this);
    this.handleUserNameInputChange = this.handleUserNameInputChange.bind(this);
    this.createPost = this.createPost.bind(this);

        this.state = {
          newPostBody: '',
          newPostUser: ''
         } 

}
  handlePostEditorInputChange(ev){
    this.setState({
      newPostBody: ev.target.value
    });
  }
  handleUserNameInputChange(ev){
    this.setState({
      newPostUser: ev.target.value
    });
  }

  createPost (){
    this.props.addPost(this.state.newPostBody, this.state.newPostUser);
    this.setState({newPostBody: ''});
  }
  render () {
    return (
        <div className="panel-body"> 
                  <div className="input-group user-name">
                    <span className="input-group-addon" id="basic-addon1">Tu nombre</span>
                    <input value={this.state.newPostUser} 
                    onChange={this.handleUserNameInputChange}  
                    type="text" className="form-control " placeholder="Usuario"/>
                  </div>

                  <textarea value={this.state.newPostBody} 
                    onChange={this.handlePostEditorInputChange} 
                    className="form-control post-editor-input" > 
                  </textarea>

                 <button
                 onClick={this.createPost}   
                 type="button" 
                 className="btn btn-success post-editor-button">
                 Enviar
                 </button>
               </div> 
    )
  } 
}

export default PostEditor;    
