 import React, { Component } from 'react';
 import Post from './Post/component/Post';
 import PostEditor from './PostEditor/component/PostEditor';
 import firebase from 'firebase/app';
 import 'firebase/database';
 import './App.css';

    class App extends Component {
      constructor(props) {
    super(props);
    // hack para asegurar que las funciones tiene el contexto del componente
    this.addPost = this.addPost.bind(this);
    this.updateLocalState = this.updateLocalState.bind(this);
    this.componentWillMount = this.componentWillMount.bind(this);
    this.state = {
        posts : [],

  }

      // config copiado de firebase ya que hemos creado ahi el proyecto
    const config = {
    apiKey: "AIzaSyA4NWdQKFSsdZx1-vl5mTX-Av4gGajuMLg",
    authDomain: "chatappreact-e62d5.firebaseapp.com",
    databaseURL: "https://chatappreact-e62d5.firebaseio.com",
    projectId: "chatappreact-e62d5",
    storageBucket: "chatappreact-e62d5.appspot.com",
    messagingSenderId: "211880137165"
  };
// referencia al objeto de firebase
    this.app = firebase.initializeApp(config);
//referencia a la base de datos
    this.database = this.app.database();
// referencia a la "tabla" post
    this.databaseRef =this.database.ref().child('post');
  }
  addPost (newPostBody, newPostUser) {
    // copiar el estado
    const postToSave = {name: newPostUser, message: newPostBody};
   // guardar en firebase nuestros posts
   this.databaseRef.push().set(postToSave);
  }

  componentWillMount() {
    const {updateLocalState} = this;
    this.databaseRef.on('child_added', snapshot => {
       const response = snapshot.val();
       updateLocalState(response);
    });
  }

  updateLocalState(response) {
   // copia del estado actual
    const posts = this.state.posts;
    // separar por saltos de linea (no se ven en html)
     //const brokenDownPost = response.message.split(/[\r\n]/g);
    // actualizar la copia del estado
  posts.push(response);
    // actualizar el estado
    this.setState(posts);
  } 

      render() {
        return (
          <div>
          <div className="panel panel-primary post-editor"> 
          <div className="panel-heading"> 
            <h3 className="panel-title">Mensajes en el chat</h3> 
          </div> 
         {
                this.state.posts.map((item, idx) => {
                  return (<Post key={idx}  userName={item.name}  postBody={item.message} />)
                })
              }
        </div>  

            <div className="panel panel-primary post-editor"> 
              <div className="panel-heading"> 
                <h3 className="panel-title">Escribe tu mensaje</h3> 
              </div> 

              <PostEditor addPost={this.addPost} />

            </div>
          </div>        );
      }
    }

    export default App;  
