import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

  // importar el modulo
  import 'jquery';
  // definirlo como variable global disponible en toda la app 
global.jQuery = require('jquery');
  // importar bootstrap (el modulo)
require('bootstrap');
  // traer el css de bootstrap en la app 
import 'bootstrap/dist/css/bootstrap.css';

import './index.css';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
